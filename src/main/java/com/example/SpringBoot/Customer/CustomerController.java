package com.example.SpringBoot.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("")
    public List<Customer> getAllCustomer() {
        return customerService.getAllCustomer();
    }

    @RequestMapping("/{custId}")
    public Customer getCustomerById (@PathVariable String custId) {
        return customerService.getCustomerById(custId);
    }

    @RequestMapping(method=RequestMethod.POST,value = "")
    public void addCustomer (@RequestBody Customer customer) {
        customerService.addCustomer(customer);
    }

    @RequestMapping(method=RequestMethod.PUT,value = "/{custId}")
    public void updateCustomer (@RequestBody Customer customer,@PathVariable String custId) {
        customerService.updateCustomer(customer,custId);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/{custId}")
    public void deleteCustomerById (@PathVariable String custId) {
        customerService.deleteCustomerById(custId);
    }

}
