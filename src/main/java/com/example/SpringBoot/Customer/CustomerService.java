package com.example.SpringBoot.Customer;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CustomerService {

    private final List<Customer> customerList = new ArrayList<>(Arrays.asList(
            new Customer("1","Sarang", "pune"),
            new Customer("2","Aniket", "nashik")
    ));
    public List<Customer> getAllCustomer() {
        return customerList;
    }

    public Customer getCustomerById (String custId) {
        return customerList.stream().filter(customer -> customer.getId().equals(custId)).findFirst().get();
    }

    public void addCustomer (Customer customer) {
        customerList.add(customer);
    }

    public void updateCustomer ( Customer customer,String custId) {
        for (int i = 0; i < customerList.size(); i++) {
            Customer custObj = customerList.get(i);
            if (custObj.getId().equals(custId)) {
                customerList.set(i,customer);
            }
        }
    }

    public void deleteCustomerById (String custId) {
        customerList.removeIf(customer -> customer.getId().equals(custId));
    }
}
